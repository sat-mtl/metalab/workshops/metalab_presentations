---
title:
- Quelques recherches sur les Haut-parleurs sphériques 
author:
- Nicolas Bouillot
institute:
- Société des Arts Technologiques [SAT]
theme:
- default
date:
- 22 octobre 2021
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---

# Structure de la présentation

* Article 1 : le haut-parleur "393"
* Article 2 : le haut-parleur "170" 


# Design and Control of Mixed-Order Spherical Loudspeaker Arrays

Stefan Riedel (Gratz, Austria) Franz Zotter (Gratz, Austria) Robert Höldrich (Gratz, Austria) 

# Le haut-parleur "393" à ordre mixte

![](images/hautparleur-mixed-order.jpg)

# Contenu de l'article

* Usage : sculpture sonore avec rebonds des sons sur les murs
* Défi : irrégularité des positions nécessite un traitement ambisonique à ordre mixe
* Les expériences avec le haut-parleur IKO montrent que c'est l'axe horizontal qui semble compter le plus
* Présentation de méthodes de calcul pour le beamforming ambisonic à ordre mixte
* Mesures de directivité

# Liens

* Filtrer + fichiers pour impression 3D : [https://github.com/stefanriedel/the-393-array](https://github.com/stefanriedel/the-393-array)
* Réponses impultionnelles (IEM OKO) [http://phaidra.kug.ac.at/o:77431](http://phaidra.kug.ac.at/o:77431)
* Réponses impultionnelles (393-1) [http://phaidra.kug.ac.at/o:77567](http://phaidra.kug.ac.at/o:77567)

# A personal, 3D printable compact spherical loudspeaker array

Valerian Drack (Graz Austria) , Franz Zotter (Graz Austria) , and Natasha Barrett (Oldo, Norwa)

# Le haut-parleur "170"

![](images/hautparleur170.jpg)

* 7 canaux à l'horizon, un subwoofer au top

# Contenu de l'article

* Usage : composition électroacoustique
* Défi : haute définition horizontale
* Bon état de l'art
* Décodeur ambisonic avec beamforming (par fréquence)
* Égalisation par mesure acoustique
* Plugin audio sous licence GPL

# Liens

* Information d'assemblage : [https://iaem.at/projekte/sphericalarrays/170-loudspeaker](https://iaem.at/projekte/sphericalarrays/170-loudspeaker)
* plugin VST : [http://www.matthiaskronlachner.com/?p=1910](http://www.matthiaskronlachner.com/?p=1910)
* Sources du plugin VST: [https://github.com/kronihias/mcfx](https://github.com/kronihias/mcfx)

# Fin !