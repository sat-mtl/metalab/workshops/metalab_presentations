This presentation give some information about the Metalab git statistics and software. More particularly it introduces the following

* Some git commands about how to get statistics
* Total and per year number of contributor, commits and tags per software
* a 10 years overview of code contribution to Metalab's public repos

The markdown presentation is [pres.md](pres.md)

Build and see the presentation
------------------------

```
# install mdp, a terminal based prentation tool
sudo apt install mdp
# Display slides
mdp pres.md
```