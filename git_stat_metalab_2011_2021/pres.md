%title: Statistique de quelques dépôts git logiciel du Metalab de 2011 à 2021
%author: Nicolas Bouillot
%date: 2021-07-30

-> # Statistique de quelques dépôts git logiciel du Metalab de 2011 à 2021 <-

--------------------------------------------------
-> # Ce qu'est un historique git <-
==============

* Dépôt logiciel git : serveur principal hébergeant du code et son historique

* Historique git : ensemble des ajouts de codes avec date et phrase de résumé

* Commit git : validation par un.e contributeur.trice d'un ajout de code dans l'historique

* Branche git : un chemin spécifique (et nommé) dans l'historique

* gitlab : service d'hébergement de dépôt git avec accès public ou privé.

--------------------------------------------------
-> # Visualisation <-
==============

```
$git log --graph --oneline

*   1b684e8 (HEAD -> master) Merge branch 'modular_synths' into 'master'
|\  
| * 906cc10 (origin/modular_synths) part 1 of the modular synths presentation
| * 5467215 add vcvrack patch
|/  
*   f9109a5 Merge branch 'shmdata' into 'master'
|\  
| *   2d4ae1c (origin/shmdata) Merge branch 'shmdata' of ...
| |\  
| | *   03df258 Merge branch 'shmdata_example' into 'shmdata'
| | |\  
| | | * 8dc4217 Added an example to shmdata introduction
| | |/  
| * / 8f903ff prensetation Shmdata
| |/  
| * ba7575b initial shmdata presentation
|/  
*   9610190 Merge branch 'some_cpp_idioms'
|\  
| * 252ad1a (origin/some_cpp_idioms) cpp idioms
|/  
* 98e3cd0 Added git rescue presentation
```

--------------------------------------------------
-> # Calcul des totaux - #commits <-
==============

Lister tous les commits accessibles :
```
git rev-list --no-merges --all --count
```

* on ne compte pas les _merge_ car ils n'ajoutent pas de nouvelle contribution de code
* on n'utilise pas git log car il liste les contributions de la branche principale uniquement

--------------------------------------------------
-> # Calcul des totaux - #versions <-
==============

Lister tous les tags, seulement dans la branche principale
```
git log --no-walk --tags --oneline | wc -l
```

* `wc -l` compte le nombre de lignes, je n'ai pas trouvé d'option pour compter avec `git log`

--------------------------------------------------
-> # Calcul des totaux - #personnes <-
==============

Lister tous les tags, seulement dans la branche principale
```
git log --no-merges --all --format='%aN' | sort | uniq -c
```

* `--format='%aN'` affiche seulement les noms des auteur.trice.s des commits
* La commande bash `sort` fait un tri alphabétique des lignes
* La commande bash `uniq` enlève les lignes dupliquées
* Problème: contributions par une même personne avec des noms différents

--------------------------------------------------
-> # Calcul des totaux - #personnes <-
==============

Filtre des auteurs (fonction bash)
```
function filter_authors {
    while read stdinput
    do
    echo $stdinput | \
        sed 's/Fran]ois Ubald Brien/François Ubald Brien/'| \
        sed 's/Vampolka$/Emile Ouellet-Delorme/' | \
        sed 's/^Chesnel$/Pierre-Antoine Chesnel/'| \
        sed 's/^zk$/Zack Settel/'| \
        sed 's/vlnk$/Valentin Laurent/' | \
        sed '/4d3d3d3$/d' | \
        sed '/OpSocket$/d' | \
        sed '/metalab$/d'
    done
}
```

Finalement pour le #personnes :
```
git log --no-merges --all  --format='%aN' | filter_authors | sort | uniq -c
```

-------------------------------------------------
-> # Statistiques par période  <-
==============

Utiliser les options `--since` et `--until` !
```
git rev-list --no-merges --all --count --since 2016-09-01 --until 2017-08-31 
```

* fonctionne aussi avec `git log`

-------------------------------------------------
-> # Statistiques des dépôts SAT <-
==============

* Les dépôts publiques, y compris ceux archivés
* Du 1er avril 2011 au 31 mars 2022
* Pas les dépôts privés, pas les dépôts éphémères
* Il en manque !

-------------------------------------------------
-> # Tada ! (Totaux des principaux dépôts actifs) <-
==============

repo                      commits   contributors   versions
----                      ----      ----           ----
EditionInSitu             656       11             0
calimiro                  183       3              4     
doc-satie                 64        5              0     
doc-shmdata               64        3              0     
doc-splash                117       3              0     
poire                     150       8              0     
satie                     1143      9              14    
shmdata                   698       12             91    
splash                    2121      9              78    
spook                     187       5              0     
switcher                  3259      21             138   
tools-ambir               163       9              0     
varays                    375       9              8     
hubs-custom-client        39        3              0     
hubs-injection-server     5         1              0     
scenic                    2540      14             54  



-------------------------------------------------
-> # Les commits dans le temps ! <-
==============

-------------------------------------------------
-> # Commits dans les projets 3D <-
==============

repo                      11/12 12/13 13/14 14/15 15/16 16/17 17/18 18/19 19/20 20/21 21/22 
----                      -----------------------------------------------------------------
spinframework             192   385   85    4     1                                         
spinic                    4                                                                 
padweb                                63    13    36    28                                  
InSituImmersiveAuthoring                                139   81                            
EditionInSitu                                                 169   108   260   115   3     
hubs-custom-client                                                                    39    
hubs-injection-server                                                                 5     









-------------------------------------------------
-> # Commits spatialisation audio <-
==============

repo                      11/12 12/13 13/14 14/15 15/16 16/17 17/18 18/19 19/20 20/21 21/22 
----                      -----------------------------------------------------------------
pdsheefa                  162   180   38    55    7     8                                   
spatosc                   235   145   30    17    41                                        
SATIE-sc                                    122   259   197   207                           
satie4blender                                           10    68          20    63    12    
satie                                                         265   418   251   197   11    
pysatie                                                       30    7     26    68    31    
satie4unity                                                   31    8     30    57          
satie4unityExample                                            116   32    11    60          
varays                                                              119   73    175   7     
tools-ambir                                                               75    20    68    
doc-satie                                                                       39    20    
poire                                                                           77    73    

-------------------------------------------------
-> # Commits interaction <-
==============

repo                      11/12 12/13 13/14 14/15 15/16 16/17 17/18 18/19 19/20 20/21 21/22 
----                      -----------------------------------------------------------------
blobserver                      134   340   3     2                                         
posturevision             63    350   230   87    215   92                                  
spook                                                                     15    119   52    

-------------------------------------------------
-> # Commits videofresque <-
==============

repo                      11/12 12/13 13/14 14/15 15/16 16/17 17/18 18/19 19/20 20/21 21/22 
----                      -----------------------------------------------------------------
splash                                341   354   420   346   182   133   111   155   71    
calimiro                                                            89    34    38    22    
doc-splash                                                                      57    60    

-------------------------------------------------
-> # Commits téléprésence <-
==============

repo                      11/12 12/13 13/14 14/15 15/16 16/17 17/18 18/19 19/20 20/21 21/22 
----                      -----------------------------------------------------------------
scenic-legacy             194   209                                                         
spinic                    4                                                                 
shmdata                   65    129   59    60    175   62    4     30    21    71    19    
switcher                        377   587   567   474   438   85    141   231   293   56    
node-switcher                   12    381   231   313   639   211   109   24    3           
scenic                          12    382   231   313   639   211   160   293   233   65    
scenic-core                     12    382   231   313   639   211   121   65    34    3     
sip-server                                              24          1           8           
ndi2shmdata                                                         14    16    3     5     
ui-components                                                       23    103   39    38    
doc-shmdata                                                                     48    16    


-------------------------------------------------
-> # Personnes pour les projets actifs <-
==============

repo                      11/12 12/13 13/14 14/15 15/16 16/17 17/18 18/19 19/20 20/21 21/22 
----                      -----------------------------------------------------------------
EditionInSitu                                                 5     4     7     7     2     
calimiro                                                            2     2     3     2     
doc-satie                                                                       1     5     
doc-shmdata                                                                     1     3     
doc-splash                                                                      1     3     
ndi2shmdata                                                         1     2     1     2     
poire                                                                           5     4     
pysatie                                                       1     1     1     4     2     
satie                                                         3     7     5     5     1     
satie4blender                                           3     2           1     2     1     
shmdata                   2     4     5     2     2     4     2     3     4     4     3     
splash                                1     2     2     2     4     3     2     3     2     
spook                                                                     2     4     4     
switcher                        3     5     3     6     6     3     8     6     6     5     
tools-ambir                                                               4     4     4     
varays                                                              4     4     6     1     
hubs-custom-client                                                                    3     
hubs-injection-server                                                                 1     
scenic                          1     3     4     6     6     3     5     4     2     3     
scenic-core                     1     3     4     6     6     3     3     4     2     2     
ui-components                                                       2     2     1     1     


-------------------------------------------------
-> # Tags (versions) des projets actifs <-
==============

repo                      11/12 12/13 13/14 14/15 15/16 16/17 17/18 18/19 19/20 20/21 21/22 
----                      -----------------------------------------------------------------
EditionInSitu                                                                               
calimiro                                                                        4           
doc-satie                                                                                   
doc-shmdata                                                                                 
doc-splash                                                                                  
ndi2shmdata                                                         1     2     1     1     
poire                                                                                       
pysatie                                                                         1           
satie                                                         2     7     2     3           
satie4blender                                                                   1     1     
shmdata                   2     6     8     1     3     19          9     4     29    10    
splash                                      6     7     15    12    4     2     20    12    
spook                                                                                       
switcher                              7     2     4     43    14    6     8     39    13    
tools-ambir                                                                                 
varays                                                              1           7           
hubs-custom-client                                                                          
hubs-injection-server                                                                       
scenic                                5     5     5     18    8     2     7     2     1     
scenic-core                           5     5     5     18    8     2     6     2     1     
ui-components                                                             1                 

-------------------------------------------------
-> # Conclusion <-
==============

Périodes :
* 2011 - 2013 : fins et renaissances des outils principaux
* 2013 - 2016 : écosystème logiciel et amélioration de la maturité des outils
* 2016 - 2021 : nouveaux outils, croissance des contributions et documentation

