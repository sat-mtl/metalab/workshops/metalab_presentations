Cette introduction couvre des concepts de programmation C++ permettant de d'organiser ses classes et leurs API, en permettant :
* d'avoir une API simple et qui réduit les erreurs d'utilisation
* de réduire les fuites mémoires 
* de structurer et d'améliorer la gestion des erreurs
* d'utiliser des instances non thread safe dans un environnement multithreadé
* effectuer des tâches periodiques RAII

On y apprend

On y trouve :
* Le principe de Resource Allocation Is Initialisation (RAII)
* 

La présentation markdown est [pres.md](pres.md)

Build and see the presentation
------------------------

```
# install mdp, a terminal based prentation tool
sudo apt install mdp
# Display slides
mdp pres.md
```
