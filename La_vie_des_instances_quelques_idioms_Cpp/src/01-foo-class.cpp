//compile with
// g++ -o 01-foo-class 01-foo-class.cpp

#include <iostream>

class Foo {
 public:
  Foo() { std::cout << "Foo()" << std::endl;}
  ~Foo() { std::cout << "~Foo()" << std::endl;}
};


int main() {
  Foo obj;
}

// prints :
// Foo()
// ~Foo()
