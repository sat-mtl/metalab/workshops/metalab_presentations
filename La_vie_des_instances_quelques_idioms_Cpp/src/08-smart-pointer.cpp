#include <iostream>
#include <memory>

class Foo {
 public:
  Foo() { std::cout << "Foo()" << std::endl;}
  ~Foo() { std::cout << "~Foo()" << std::endl;}
  void hello() const { std::cout << "hello" << std::endl; }
};


int main() {
  auto obj = std::make_unique<Foo>();
  // obj can be used as a raw pointer
  obj->hello();
}

// prints :
// Foo()
// hello
// ~Foo()
