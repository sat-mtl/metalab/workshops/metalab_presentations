#include <iostream>

class FileWriter {
 public:
  // constructor
  FileWriter() { std::cout << "open" << std::endl; }
  ~FileWriter(){ std::cout << "close" << std::endl; }
  bool write(const std::string& val) { return val.empty() ? false: true; }
};

int main(){
  FileWriter j;
  if (!j.write("")) exit(1);
}

