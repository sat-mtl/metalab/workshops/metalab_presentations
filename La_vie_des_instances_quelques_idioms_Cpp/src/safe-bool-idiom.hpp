class SafeBoolIdiom {
 public:
  virtual ~SafeBoolIdiom() {}
  explicit operator bool() const;

 private:
  virtual bool safe_bool_idiom() const = 0;
};

SafeBoolIdiom::operator bool() const { return safe_bool_idiom(); }
