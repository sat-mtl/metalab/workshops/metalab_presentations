#include <iostream>

class Foo {
 public:
  Foo() { std::cout << "Foo()" << std::endl;}
  ~Foo() { std::cout << "~Foo()" << std::endl;}
};


int main() {
  Foo* obj = new Foo;
  delete obj;
}

// prints :
// Foo()
// ~Foo()
