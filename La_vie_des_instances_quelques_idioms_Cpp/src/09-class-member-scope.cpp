#include <iostream>

class Foo{
public :
  Foo(int val) : val_(val) { std::cout << "Foo()" << std::endl; }
  ~Foo() { std::cout << "~Foo()" << std::endl; }

private:
  int val_;
};

class Bar{
public :
  Bar() { std::cout << "Bar()" << std::endl; }
  ~Bar() { std::cout << "~Bar()" << std::endl; }

private:
  Foo foo_{1};
};

int main() {
  Bar bar;
}

// prints:
// Foo()
// Bar()
// ~Bar()
// ~Foo()
