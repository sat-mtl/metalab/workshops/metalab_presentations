%title: Git Rescue Tips
%author: Emmanuel Durand
%date: 2020-10-16

-> ┏━╸╻╺┳╸   ┏━┓┏━╸┏━┓┏━╸╻ ╻┏━╸   ╺┳╸╻┏━┓┏━┓ <-
-> ┃╺┓┃ ┃    ┣┳┛┣╸ ┗━┓┃  ┃ ┃┣╸     ┃ ┃┣━┛┗━┓ <-
-> ┗━┛╹ ╹    ╹┗╸┗━╸┗━┛┗━╸┗━┛┗━╸    ╹ ╹╹  ┗━┛ <-

-------------------------------

-> Git Rescue Tips <-

Tips and info to get back to a working Git repository

* Good to know basics
* Best practices when using Git
* How to get out of common uncomfortable situations

-------------------------------

-> Basics: what is a commit? <-

* Difference between the previous state and the current one
* Does not necessarily hold a complete file
* Is referenced by a hash (SHA-256) in git
* Knows it's ancestor

-------------------------------

-> Basics: branches? <-

* References a specific commit
* ... that's pretty much it

-------------------------------

-> Basics: commits (again) <-

* A commit can exist by itself
* ... even when not included in any branch

-------------------------------

-> Basics: what is a rebase? <-

* A rebase goes through a list of commits to modify them
* For example:

    git rebase -i main

  will replay the history from the closest common point with the main branch

* This will create new commits based on the original ones

-------------------------------

-> Basics: what is a rebase? <-



`main`                        A -- B -- C -- D
                                      |
`feature/awesome`                       + -- E -- F

-------------------------------

-> Basics: what is a rebase? <-



`main`                        A -- B -- C -- D
                                           |
`feature/awesome`                            + -- E' -- F'


-------------------------------

-> Best practices <-

* keep commits elementary: split up big tasks
* rebase often on the `develop` branch to prevent too many conflicts

-------------------------------

-> Dividing commits <-

* A great tool to help keeping things clean:

    git add -p

* To tidy up previously-created commits:

    git rebase -i [target branch or commit]
    git reset --soft HEAD~1

-------------------------------

-> Piling up branches <-

* Sometime, a MR depends on another MR
* `git rebase -i` can be tricky to use
* Keep in mind that when rebasing, new commits are created

-------------------------------

-> Where are my changes? <-

* When rebasing, or destroying branches, changes can get lost
* If discovered soon enough, this can save you:

    git reflog

-------------------------------

-> Why does it not work anymore? <-

* Sometimes, code which used to work does not anymore
* First, check your unit tests, they might give you useful data
* If not successful:

    git bisect

-------------------------------

-> Any other specific cases? <-
