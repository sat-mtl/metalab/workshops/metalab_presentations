Git Rescue Tips
===============

This presentation gives some tips to get out of some usual issues when using git

The markdown presentation is [slides.md](./slides.md)

Build and see the presentation
------------------------------

```
# install mdp, a terminal based prentation tool
sudo apt install mdp
# Display slides
mdp slides.md
```
