#include <thread>
#include <mutex>

using namespace std::chrono_literals;

std::mutex mut; 
void do_something() {
  mut.lock();
  while (true)
    std::this_thread::sleep_for(2s);
}

int main() {
  std::thread t(do_something);
  std::this_thread::sleep_for(100ms);
  mut.lock();
}


