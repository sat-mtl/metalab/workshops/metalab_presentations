int add(int a, int b) {
  return a + b;
}

int main() {
  int result = 0;
  while (result != 10) {
    result = add(result, 1);
  }
}
