Using perf, plus other tools
============================

`perf` is great, but navigating in its output can be a pain. Fortunately there are some great tools to help making sense of the data. In this use case, we will play with `gprof2dot` and `flamegraph` which offer two different albeit complementary views of the profiling measurements.


Example: return of the OBJ loader
---------------------------------

For this example, we will go back to the OBJ loader. Even though we found the origin of the slowdown based on experience, it would be useful to detect such issues without having the rely necessarily on experience. Let's build and record the profiling data:

```bash
./build.sh
mperf record -F 999 -g ./main ../assets/satosphere.obj
mperf script > out.perf
```


gprof2dot
---------

[gprof2dot](https://github.com/jrfonseca/gprof2dot) is a Python script which converts the output of various profilers, including `perf`, into a dot graph. The color of the node denotes the time spent executing the associated function. Let's run it on our previous measurements:

gprof2dot:
```bash
mperf script | c++filt | gprof2dot -f perf | dot -Tpng -o output.png
```

And here is a portion of the resulting `output.png` file:

![gprof2dot output](./result_gprof2dot.png)

We can see clearly that the constructor of `std::regex` takes as much as 82% of the time. Let's make them static, or part of the class!


flamegraph
----------

[flamegraph](https://github.com/brendangregg/FlameGraph) gives another view of the call graph. It is organized as a stack of function, the bottom line being the call to `main`, then functions called by `main`, then functions called by these functions, etc. Let's run it on our measurements:

flamegraph:
```bash
../external/flamegraph/stackcollapse-perf.pl out.perf > out.folded
../external/flamegraph/flamegraph.pl out.folded > kernel.svg
```

And the resulting image:

![flamegraph output](./result_flamegraph.png)

Obviously the results are the same.
