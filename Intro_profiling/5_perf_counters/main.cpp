#include <chrono>
#include <iostream>
#include <list>
#include <vector>

int main()
{
    const size_t count = 1 << 20;

    std::list<float> numberList;

    // Fill structure with numbers
    for (size_t i = 0; i < count; ++i)
        numberList.push_back(static_cast<float>(i));

    volatile float value;
    std::cout << "Looping through std::list<float> -> " << std::flush;
    auto start = std::chrono::steady_clock::now();
    for (const auto& num : numberList)
        value = num;
    auto end = std::chrono::steady_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    std::cout << duration << "µs\n";
}
