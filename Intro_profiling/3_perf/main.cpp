#include "../src/dense_map.h"

#include <chrono>
#include <iostream>
#include <map>
#include <unordered_map>
#include <vector>

int main()
{
    const size_t count = 1 << 10;
    const size_t loopCount = 1 << 8;
    volatile int key;
    volatile float value;

    std::cout << "----> DenseMap performance test\n";

    /**
     * DenseMap
     */
    std::cout << "DenseMap::insert -> " << std::flush;
    DenseMap<int, float> dmap{};
    auto start = std::chrono::steady_clock::now();
    for (int loop = 0; loop < loopCount; ++loop)
    {
        dmap.clear();
        for (size_t i = 0; i < count; ++i)
            dmap.insert({i, static_cast<float>(i)});
        for (size_t i = 0; i < count; ++i)
            dmap.insert({i, static_cast<float>(i)});
    }
    auto end = std::chrono::steady_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    std::cout << duration << "µs\n";

    std::cout << "DenseMap::iterator -> " << std::flush;
    start = std::chrono::steady_clock::now();
    for (int loop = 0; loop < loopCount; ++loop)
        for (const auto& entry : dmap)
        {
            key = entry.first;
            value = entry.second;
        }
    end = std::chrono::steady_clock::now();
    duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    std::cout << duration << "µs\n";
}
