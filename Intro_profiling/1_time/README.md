Using time
==========

`time` is a tool which gives the CPU and user times consumed by the command given as parameter:
- user time is the duration the process was running, as perceived by the user
- CPU time is the total duration the process was being executed by the process. It may be longer or shorter then the user time, depending on whether the process was waiting for some events, or if it is multithreaded.

`time` allows for benchmarking a whole process and does not give any insight on which part of the process took time. It can be used to profile small pieces.

Example: OBJ loader
-------------------

This example is an excerpt from [SLAPS](https://gitlab.com/sat-metalab/slaps), and handles the loading of OBJ files.

Compile and test:

```bash
./build.sh
time ./main ../assets/satosphere.obj
```

Discussion
----------

This code takes a unusually long time to load a 20k faces model. There is not much loops or computation which could justify that, but we can see that `std::regex` is used to parse each line. Regular expressions can be slow but the example takes around 15 seconds on a modern CPU to load the model, which gives around 4 lines parsed per ms (the OBJ file is 53260 lines long): that's too slow.
It turns out the real issue is the fact that the `std::regex` are recompiled at each call of `Obj::loadCoordinates` and `Obj::loadFaces`. Try declaring them as `static`, and test again to see the difference.
