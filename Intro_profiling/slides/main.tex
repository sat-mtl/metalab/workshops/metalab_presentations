\documentclass[11pt, aspectratio=169]{beamer}
\usepackage{times}
\usepackage{verbatim}
\usepackage[french,english]{babel}
\usepackage{iflang}
%\usepackage{latin1}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{array}
\usepackage{media9}
\usepackage{enumitem}
\usepackage{gensymb}
\usepackage{graphicx}
\usepackage{multimedia}
\usepackage{wasysym}
\usepackage[super]{nth}
\usepackage{ulem}

% \usetheme{Warsaw}
\usefonttheme{structuresmallcapsserif}
\usecolortheme[RGB={150,150,150}]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamercolor{normal text}{fg=white!80!black,bg=black}
\setbeamercolor{alerted text}{fg=red!80!black}

% Add an image
\newcommand{\embedimage}[2]{
  \includegraphics[width=11cm,height=#1,keepaspectratio]{#2}
}

\title{Introduction to profiling}
\author{Emmanuel Durand}
\date{7 août 2019}

% ╺┳┓┏━┓┏━╸╻ ╻┏┳┓┏━╸┏┓╻╺┳╸
%  ┃┃┃ ┃┃  ┃ ┃┃┃┃┣╸ ┃┗┫ ┃ 
% ╺┻┛┗━┛┗━╸┗━┛╹ ╹┗━╸╹ ╹ ╹ 
\begin{document}

\maketitle

\frame[plain]
{
    \frametitle{Performance problems?}
    \centering\embedimage{6cm}{images/coccinelle.png}
}

\frame[plain]
{
    \frametitle{Summary}

    What this introduction is about~:\\
    \begin{itemize}
      \item[-]{profiling tools}
      \item[-]{some best practices}
      \item[-]{leads to go further}
    \end{itemize}

    What this introduction is not about~:\\
    \begin{itemize}
      \item[-]{ready to use tools to improve performances}
      \item[-]{an introduction to multithreading}
    \end{itemize}
}

\frame[plain]
{
    \frametitle{Profiling ?}
    "In software engineering, profiling [...] is a form of dynamic program analysis that measures, for example, the space (memory) or time complexity of a program, the usage of particular instructions, or the frequency and duration of function calls. Most commonly, profiling information serves to aid program optimization." \\
    ~\\
    \small\url{https://en.wikipedia.org/wiki/Profiling_(computer_programming)}
}

\frame[plain]
{
    \frametitle{Sources}

    All sources, from this presentation and the various examples, are here:\\
    ~\\
    \small\url{https://gitlab.com/sat-metalab/tools/intro_profiling}
}

\frame[plain]
{
    \frametitle{time}

    `time` is a tool which gives the CPU and user times consumed by the command given as parameter:\\
    \begin{itemize}
      \item[-]{user time is the duration the process was running, as perceived by the user}
      \item[-]{CPU time is the total duration the process was being executed by the process. It may be longer or shorter then the user time, depending on whether the process was waiting for some events, or if it is multithreaded.}
    \end{itemize}
}

\frame[plain]
{
    \frametitle{time}

    \centering\Large Example: loading an OBJ file\\
    ~\\
    \centering\large (see ./1\_time/README.md)
}

\frame[plain]
{
    \frametitle{time - discussion}

    \begin{itemize}
      \item[-] the 3D model has 20k faces: loading it is far too long\dots
      \item[-] \dots but regular expressions are not known for their speed\dots
      \item[-] \dots and we recompile them at each loop.
    \end{itemize}
}

\frame[plain]
{
    \frametitle{std::chrono}

    `std::chrono` is to poor man solution to profile specific part of a code:\\
    \begin{itemize}
      \item[-]{good way to compare multiple implementation of a given function}
      \item[-]{not so good to pinpoints which function brings the performances down}
    \end{itemize}
}

\frame[plain]
{
    \frametitle{std::chrono}

    \centering\Large Example: various map implementations\\
    ~\\
    \centering\large (see ./2\_chrono/README.md)
}

\frame[plain]
{
    \frametitle{std::chrono - discussion}

    \begin{itemize}
      \item[-]{this is not a real world test, as the process as one thing to do: benchmark}
      \item[-]{the compiler may optimize out code without telling us it did}
      \item[-]{this is C++, so performances vary greatly depending on the optimization flag}
    \end{itemize}
}

\frame[plain]
{
    \frametitle{perf}

    \begin{itemize}
      \item[-]{full fledged profiler bundled with the Linux kernel}
      \item[-]{has access to specific performance counters}
      \item[-]{samples the profiled process at regular intervals}
    \end{itemize}
}

\frame[plain]
{
    \frametitle{perf}

    \centering\Large Example: profiling DenseMap\\
    ~\\
    \centering\large (see ./3\_perf/README.md)
}

\frame[plain]
{
    \frametitle{perf - discussion}

    \begin{itemize}
      \item[-]{name demangling is a must, but can be hard to get working}
      \item[-]{optimization level still has a big impact on the result: make sure to test with the production flags!}
    \end{itemize}
}

\frame[plain]
{
    \frametitle{perf and tools}

    `perf` output may/will be hard to decypher. Here come to the rescue:\\
    \begin{itemize}
      \item[-]{gprof2dot which generates a callgraph}
      \item[-]{flamegraphs}
    \end{itemize}
}

\frame[plain]
{
    \frametitle{perf and tools}

    \centering\Large Example: return of the OBJ loader\\
    ~\\
    \centering\large (see ./4\_perf\_n\_tools/README.md)
}

\frame[plain]
{
    \frametitle{performance counters}

    `perf` has access to a lot of Linux kernel's internal counters:\\
    \begin{itemize}
      \item[-]{instructions executed per cycle}
      \item[-]{cache misses}
      \item[-]{missed branch prediction}
      \item[-]{\dots}
    \end{itemize}
}

\frame[plain]
{
    \frametitle{performance counters}

    \centering\Large Example: about contiguous memory\\
    ~\\
    \centering\large (see ./5\_perf\_counters/README.md)
}

\frame[plain]
{
    \frametitle{performance counters - discussion}

    \centering\embedimage{6cm}{./images/cpu_cache.png}
}

\frame[plain]
{
    \frametitle{Going forward\dots}

    \begin{itemize}
      \item[-]{GCC/Clang address sanitizer (-fsanitize=address)}
      \item[-]{GCC/Clang thread sanitizer (-fsanitize=thread)}
      \item[-]{Valgrind}
    \end{itemize}
}

\end{document}
