# Présentation shmdata

Pour compiler la présentation, il faut rouler le script compile.sh. Celui-ci télécharge et installe localement reveal.js, puis génère la présentation avec Pandoc.

Pour installer Pandoc
```
sudo apt install pandoc
```

Plus de détails ici sur reveal.js: https://github.com/jgm/pandoc/wiki/Using-pandoc-to-produce-reveal.js-slides

