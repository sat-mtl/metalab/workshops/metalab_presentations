#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

import argparse
import pyshmdata
import time
from typing import Any, Dict

quit = False

parser = argparse.ArgumentParser()
parser.add_argument("--interesting")
args = parser.parse_args()

interesting_value = args.interesting or "yes"


def callback(user_data: Any, buffer: bytes, datatype: str, parsed_datatype: Dict[str, Any]):
    global quit
    data = buffer.decode(encoding="utf-8")

    if parsed_datatype.get("interesting", "no") == interesting_value:
        print(f"Received text buffer \"{data}\"")
    else:
        print("Data does not meet our expectations, it won't be displayed")

    if data == "quit":
        print("\"quit\" received, exiting")
        quit = True


reader = pyshmdata.Reader(path="/tmp/pyshmdata_example", callback=callback, user_data=quit)

print("Waiting for data from the reader")

while True:
    time.sleep(0.1)
    if quit:
        break
