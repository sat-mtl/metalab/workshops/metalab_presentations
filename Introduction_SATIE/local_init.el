

;;; init:
;; This is a custom init file to allow running SuperCollider code from OrgMode

;;; Code:
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ("gnu" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(package-refresh-contents)

(package-reinstall 'zenburn-theme)

(require 'org-install)

(org-babel-load-file (expand-file-name "README.org" default-directory))
